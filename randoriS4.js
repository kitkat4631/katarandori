/EN ENTREE on va créer une FONCTION qui recupere une STRING constituée de CHIFFRE séparé par des ESPACES en ENTREE

// EN SORTIE  renvoie un STRING contenant le NOMBRE le PLUS HAUT ainsi que le nombre le PLUS BAS 




function maxEtMin(string)
{
  var tab = string.split(" ");

  var min = Math.min.apply(Math, tab);
  var max = Math.max.apply(Math, tab);

  return max + " " + min;
}



console.log(maxEtMin("1 3 5 7 9"));
console.log(maxEtMin("1 2 3 4 5")); // return "5 1"
console.log(maxEtMin("1 2 -3 4 5")); // return "5 -3"
console.log(maxEtMin("1 9 3 4 -5")); // return "9 -5"