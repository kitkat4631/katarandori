// En Javascript, écrire une fonction “masquer()” telle que :
// 1 string en entrée
// remplace tous les caractères sauf les 4 derniers par “#”
// ```
// abcdef => ##cdef
// Hello world! => ########rld!
// ```
var entree = "abcdef";
function masquer(entree)
{
    var aGarder = entree.substring((entree.length - 4), entree.length);
    var aCacher = entree.substring(0, (entree.length -4));
    var compteur = aCacher.length;
    
    var crypter = '#'.repeat(compteur);

    var sortie = crypter + aGarder;
    return sortie;

    console.log(crypter);
    console.log(aGarder);
    console.log(aCacher);
    console.log(compteur);

}

console.log(masquer(entree));


