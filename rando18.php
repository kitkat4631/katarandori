<?php 
// Given "listen" and a list of candidates like "enlists" "google" "inlets" "banana" the program should return a list containing "inlets".
$mot1 = "listen";
$mot2 = "inlets";
 
function isAnagram($mot1, $mot2)
{
    $lettreMot1 = array();
    $lettreMot2 = array();
    $isAnagram = true;
    if (strlen($mot1) == strlen($mot2))
    {
        foreach(str_split($mot1) as $char)
        {
            if($char == ' ')
            {
                continue;
            }
            else if(!isset($lettreMot1[$char]))
            {
                $lettreMot1[$char] = 0;
                continue;
            }
            $lettreMot1[$char]++;
        }
        foreach(str_split($mot2) as $char)
        {
            if(!isset($lettreMot2[$char]))
            {
                $lettreMot2[$char] = 0;
                continue;
            }
            $lettreMot2[$char]++;
        }
        foreach($lettreMot1 as $key => $value)
        {
            if(!isset($lettreMot2[$key]) || $lettreMot2[$key] !== $lettreMot1[$key])
            {
                $isAnagram = false;
                break;
            }
        }
    }
    else
    {
        $isAnagram = false;
    }
 
    return $isAnagram;
}
if(isAnagram($mot1,$mot2))
{
    echo 'c\'est un anagramme';
}
else
{
    echo 'ce n\'est pas un anagramme';
}

?>