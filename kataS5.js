//expandedFirstForm(12); // Should return '2*2*3'
//expandedFirstForm(42); // Should return '2*3*7'
//expandedFirstForm(240); // Should return '2*2*3*2*3*7*2*2*2*2*3*5'  
//Rédigez une fonction expandedFirstForm qui développe un nombre en un produit de facteurs premiers.


const nbrpremier = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29];
var resultat = [];


function expandedFirstForm(entree) {

for (var i = 0; i < nbrpremier.length; i++) {
  while (entree % nbrpremier[i] == 0) {
    entree = entree / nbrpremier[i];
    resultat.push(nbrpremier[i]);
  }
}
  return resultat.join('*');
}

console.log(expandedFirstForm(12));
console.log(expandedFirstForm(42));
console.log(expandedFirstForm(240));

console.log(expandedFirstForm(999999999999999999999999999999999));