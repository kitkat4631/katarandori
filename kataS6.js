
function expandedForm(nombre)
{
  var tab = nombre.toString().split('');

  var compteur = tab.length;
  var resultat = [];

  for (var i = 0; i < tab.length ; i ++) {
    compteur--;

    if(tab[i]>0)
    {
      if(tab[i]==1){
        resultat.push( '1' + '0'.repeat(compteur) );
      }else{
        resultat.push( tab[i] + '*1' + '0'.repeat(compteur) );
      }
    }
/*
    switch(tab[i]){
      case '0': 
        break;

      case '1':
        resultat.push( '1' + '0'.repeat(compteur) );
        break;

      default:
        resultat.push( tab[i] + '*1' + '0'.repeat(compteur) );
    }
*/
  }
  return resultat.join(' + ');
}


console.log( expandedForm(12) ); // return '10 + 2*1'
console.log( expandedForm(42) ); // return '4*10 + 2*1'
console.log( expandedForm(70304) ); // return '7*10000 + 3*100 + 4*1'